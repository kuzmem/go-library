package main

import (
	"context"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/kuzmem/go-library/library/handlers"
	"gitlab.com/kuzmem/go-library/library/models"
	"gitlab.com/kuzmem/go-library/library/postgres"
	"gitlab.com/kuzmem/go-library/library/repo"
	"gitlab.com/kuzmem/go-library/library/service"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

const DATABASE_URL = "postgres://postgres:postgres@localhost:5433/library?sslmode=disable"

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func main() {
	port := ":8080"
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	conn, err := postgres.NewConn(DATABASE_URL)
	if err != nil {
		fmt.Println(err)
	}

	authorRepo := repo.NewAuthorRepo(conn)
	err = authorRepo.CreateTable()
	if err != nil {
		fmt.Printf("abb: %s", err)
	}

	for i := 0; i < 10; i++ {
		author := models.Author{
			FirstName: gofakeit.FirstName(),
			LastName:  gofakeit.LastName(),
		}

		err := authorRepo.CreateAuthor(author.FirstName, author.LastName)
		if err != nil {
			log.Fatal("Failed to create author:", err)
		}
	}

	userRepo := repo.NewUserRepo(conn)
	for i := 0; i < 51; i++ {
		user := models.User{
			Username:  gofakeit.Username(),
			FirstName: gofakeit.Name(),
		}
		err := userRepo.CreateUser(user.Username, user.FirstName)
		if err != nil {
			log.Fatal("Failed to create user:", err)
		}
	}

	bookRepo := repo.NewBookRepo(conn)

	for i := 0; i < 100; i++ {
		book := models.Book{
			Title:     gofakeit.Sentence(5),
			Author_ID: gofakeit.Number(1, 10),
			User_ID:   gofakeit.Number(1, 50),
		}

		err := bookRepo.CreateBook(book.Title, book.User_ID, book.Author_ID)
		if err != nil {
			log.Fatal("Failed to create book:", err)
		}
	}

	libraryService := service.NewLibrary(authorRepo, bookRepo, userRepo)

	controller := handlers.NewController(libraryService)

	r.Post("/users", controller.UserCreate)
	r.Post("/authors", controller.AuthorCreate)
	r.Post("/books", controller.CreateBook)
	r.Get("/users", controller.GetUserList)
	r.Get("/authors", controller.GetAuthorList)
	r.Get("/books", controller.GetAllBooks)
	r.Get("/books/{id}", controller.GetBookByID)
	r.Post("/books/take", controller.TakeBook)
	r.Post("/books/return", controller.ReturnBook)
	r.Get("/authors/top", controller.GetTopAuthors)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	// r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
	// 	http.StripPrefix("/public/", http.FileServer(http.Dir("./"))).ServeHTTP(w, r)
	// })
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	log.Println("Starting server on :8080...")
	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

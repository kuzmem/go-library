package service

import (
	"fmt"
	"gitlab.com/kuzmem/go-library/library/models"
	"gitlab.com/kuzmem/go-library/library/repo"
)

type Library struct {
	authorRepo repo.AuthorRepoer
	bookRepo   repo.BookRepoer
	userRepo   repo.UserRepoer
}

type Libraryer interface {
	CreateAuthor(firstname, lastname string) error
	CreateUser(username, firstname string) error
	CreateBook(title string, authorID, userID int) error
	GetUserList() ([]models.UserWithBooks, error)
	GetAuthorList() ([]models.AuthorWithBooks, error)
	GetAllBooks() ([]models.BookWithAuthor, error)
	GetBookByID(id int) (models.BookWithAuthor, error)
	TakeBook(bookID, userID int) error
	ReturnBook(bookID int) error
	GetTopAuthors() ([]models.Author, error)
}

func NewLibrary(authorRepo repo.AuthorRepoer, bookRepo repo.BookRepoer, userRepo repo.UserRepoer) *Library {
	return &Library{
		authorRepo: authorRepo,
		bookRepo:   bookRepo,
		userRepo:   userRepo,
	}
}

func (l *Library) CreateAuthor(firstname, lastname string) error {
	err := l.authorRepo.CreateAuthor(firstname, lastname)
	if err != nil {
		return err
	}
	return nil
}

func (l *Library) CreateUser(username, firstname string) error {
	err := l.userRepo.CreateUser(username, firstname)
	if err != nil {
		return err
	}
	return nil
}

func (l *Library) CreateBook(title string, authorID, userID int) error {
	err := l.bookRepo.CreateBook(title, userID, authorID)
	if err != nil {
		return err
	}
	return nil
}
func (l *Library) GetUserList() ([]models.UserWithBooks, error) {
	users, err := l.userRepo.GetAllUsers()
	if err != nil {
		return nil, fmt.Errorf("can't get all users: %s", err.Error())
	}

	usersWithBooks := make([]models.UserWithBooks, 0, len(users))

	for _, user := range users {
		RentedBooks, err := l.bookRepo.GetBooksByUserID(user.ID)
		if err != nil {
			return nil, fmt.Errorf("can't get books by user_id: %s", err.Error())
		}
		usersWithBooks = append(usersWithBooks, models.UserWithBooks{User: user, RentedBooks: RentedBooks})
	}

	return usersWithBooks, nil
}

func (l *Library) GetAuthorList() ([]models.AuthorWithBooks, error) {
	authors, err := l.authorRepo.GetAllAuthors()
	if err != nil {
		return nil, fmt.Errorf("can't get all authors: %s", err.Error())
	}

	authorsWithBooks := make([]models.AuthorWithBooks, 0, len(authors))

	for _, author := range authors {
		Books, err := l.bookRepo.GetBooksByAuthorID(author.ID)
		if err != nil {
			return nil, fmt.Errorf("can't get books by author_id: %s", err.Error())
		}
		authorsWithBooks = append(authorsWithBooks, models.AuthorWithBooks{Author: author, Books: Books})
	}

	return authorsWithBooks, nil

}

func (l *Library) GetTopAuthors() ([]models.Author, error) {
	authors, err := l.authorRepo.GetTopAuthors()
	if err != nil {
		return nil, fmt.Errorf("can't get top authors: %s", err.Error())
	}
	return authors, nil
}

func (l *Library) GetAllBooks() ([]models.BookWithAuthor, error) {
	books, err := l.bookRepo.GetAllBooks()
	if err != nil {
		return nil, fmt.Errorf("failed to get all books %s", err.Error())
	}

	var booksWithAuthor []models.BookWithAuthor

	for _, book := range books {
		author, err := l.authorRepo.GetAuthorByID(book.Author_ID)
		if err != nil {
			return nil, fmt.Errorf("failed to get author for book (ID: %d): %s", book.ID, err.Error())
		}
		bookWithAuthor := models.BookWithAuthor{
			Book:   book,
			Author: author,
		}
		booksWithAuthor = append(booksWithAuthor, bookWithAuthor)
	}

	return booksWithAuthor, nil

}

func (l *Library) GetBookByID(id int) (models.BookWithAuthor, error) {
	book, err := l.bookRepo.GetBookByID(id)
	if err != nil {
		return models.BookWithAuthor{}, fmt.Errorf("failed to get book: %s", err.Error())
	}

	if book.User_ID != 0 {
		return models.BookWithAuthor{}, fmt.Errorf("book is already borrowed by user (ID: %d)", book.User_ID)
	}

	author, err := l.authorRepo.GetAuthorByID(book.Author_ID)
	if err != nil {
		return models.BookWithAuthor{}, fmt.Errorf("failed to get author for book (ID: %d): %s", book.ID, err.Error())
	}

	bookWithAuthor := models.BookWithAuthor{
		Book:   book,
		Author: author,
	}

	return bookWithAuthor, nil

}

func (l *Library) TakeBook(bookID, userID int) error {
	book, err := l.bookRepo.GetBookByID(bookID)
	if err != nil {
		return fmt.Errorf("failed to get book: %s", err.Error())
	}

	if book.User_ID != 0 {
		return fmt.Errorf("book is already borrowed by user (ID: %d)", book.User_ID)
	}

	err = l.bookRepo.TakeBook(bookID, userID)
	if err != nil {
		return fmt.Errorf("failed to update book user: %s", err.Error())
	}

	return nil

}

func (l *Library) ReturnBook(bookID int) error {
	book, err := l.bookRepo.GetBookByID(bookID)
	if err != nil {
		return fmt.Errorf("failed to get book: %s", err.Error())
	}

	if book.User_ID == 0 {
		return fmt.Errorf("book is already returned")
	}

	err = l.bookRepo.ReturnBook(bookID)
	if err != nil {
		return fmt.Errorf("failed to return book: %s", err.Error())
	}

	return nil

}

package models

type Book struct {
	ID        int    `json:"id" db:"id"`
	Title     string `json:"title" db:"title"`
	Author_ID int    `json:"author_id" db:"author_id"`
	User_ID   int    `json:"user_id" db:"user_id"`
}

type BookWithAuthor struct {
	Book   Book
	Author Author
}

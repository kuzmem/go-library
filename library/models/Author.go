package models

type Author struct {
	ID        int    `json:"id" db:"id"`
	FirstName string `json:"firstName" db:"firstname"`
	LastName  string `json:"lastName" db:"lastname"`
	BooksRead int    `json:"booksRead"`
}

type AuthorWithBooks struct {
	Author
	Books []Book
}

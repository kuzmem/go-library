package models

type User struct {
	ID        int    `json:"id" db:"id"`
	Username  string `json:"username" db:"username"`
	FirstName string `json:"firstName" db:"firstname"`
}

type UserWithBooks struct {
	User
	RentedBooks []Book
}

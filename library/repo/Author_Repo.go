package repo

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"gitlab.com/kuzmem/go-library/library/models"
	"sync"
)

type AuthorRepo struct {
	db *pgx.Conn
	*sync.Mutex
}

type AuthorRepoer interface {
	CreateAuthor(firstname, lastname string) error
	GetAuthorByID(id int) (models.Author, error)
	GetAllAuthors() ([]models.Author, error)
	GetTopAuthors() ([]models.Author, error)
}

func NewAuthorRepo(db *pgx.Conn) *AuthorRepo {
	return &AuthorRepo{
		db:    db,
		Mutex: &sync.Mutex{},
	}
}

func (a *AuthorRepo) CreateAuthor(firstname, lastname string) error {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	_, err := a.db.Exec(context.Background(),
		"INSERT INTO authors (firstname, lastname) VALUES ($1, $2)",
		firstname, lastname)
	if err != nil {
		return err
	}
	return nil
}

func (a *AuthorRepo) GetAuthorByID(id int) (models.Author, error) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	var author models.Author

	row := a.db.QueryRow(context.Background(),
		"SELECT author_id, firstname, lastname FROM authors WHERE author_id = $1", id)

	if err := row.Scan(&author.ID, &author.FirstName, &author.LastName); err != nil {
		return author, fmt.Errorf("can't scan author: %s", err.Error())
	}
	return author, nil
}

func (a *AuthorRepo) GetAllAuthors() ([]models.Author, error) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	rows, err := a.db.Query(context.Background(),
		"SELECT author_id, firstname, lastname FROM authors")
	if err != nil {
		return nil, fmt.Errorf("can't select all users: %s", err.Error())
	}
	defer rows.Close()
	var result []models.Author

	for rows.Next() {
		author := models.Author{}
		if err := rows.Scan(&author.ID, &author.FirstName, &author.LastName); err != nil {
			return nil, fmt.Errorf("can't scan author: %s", err.Error())
		}
		result = append(result, author)
	}
	return result, nil
}
func (a *AuthorRepo) GetTopAuthors() ([]models.Author, error) {
	var authors []models.Author

	query := `
		SELECT a.author_id, a.firstname, a.lastname, COUNT(*) AS books_read
		FROM authors AS a
		JOIN books AS b ON a.author_id = b.author_id
		WHERE b.user_id IS NOT NULL
		GROUP BY a.author_id, a.firstname, a.lastname
		ORDER BY books_read DESC
		LIMIT 10
	`

	rows, err := a.db.Query(context.Background(), query)
	if err != nil {
		return nil, fmt.Errorf("failed to get top authors: %s", err)
	}
	defer rows.Close()

	for rows.Next() {
		var author models.Author
		err := rows.Scan(&author.ID, &author.FirstName, &author.LastName, &author.BooksRead)
		if err != nil {
			return nil, fmt.Errorf("failed to scan row: %s", err)
		}
		authors = append(authors, author)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("error iterating rows: %s", err)
	}

	return authors, nil
}

func (a *AuthorRepo) CreateTable() error {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	query := `
CREATE TABLE IF NOT EXISTS authors (
    author_id SERIAL PRIMARY KEY, 
    firstname VARCHAR, 
    lastname VARCHAR
);

CREATE TABLE IF NOT EXISTS users (
    user_id SERIAL PRIMARY KEY,
    username VARCHAR,
    firstname VARCHAR
);

CREATE TABLE IF NOT EXISTS books (
    books_id SERIAL PRIMARY KEY,
    title VARCHAR,
    user_id INTEGER DEFAULT 0,
    author_id INTEGER,
    FOREIGN KEY (author_id) REFERENCES authors(author_id)
);
`

	_, err := a.db.Exec(context.Background(), query)
	if err != nil {
		return err
	}

	return nil
}

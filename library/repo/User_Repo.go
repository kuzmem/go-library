package repo

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"gitlab.com/kuzmem/go-library/library/models"
	"sync"
)

type UserRepo struct {
	db *pgx.Conn
	*sync.Mutex
}

type UserRepoer interface {
	CreateUser(username, firstname string) error
	GetUserByID(id int) (models.User, error)
	GetAllUsers() ([]models.User, error)
}

func NewUserRepo(db *pgx.Conn) *UserRepo {
	return &UserRepo{
		db:    db,
		Mutex: &sync.Mutex{},
	}
}

func (u *UserRepo) CreateUser(username, firstname string) error {
	u.Mutex.Lock()
	defer u.Mutex.Unlock()
	_, err := u.db.Exec(context.Background(),
		"INSERT INTO users (username, firstname) VALUES ($1, $2)",
		username, firstname)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (u *UserRepo) GetUserByID(id int) (models.User, error) {
	u.Mutex.Lock()
	defer u.Mutex.Unlock()
	var user models.User
	row := u.db.QueryRow(context.Background(),
		"SELECT user_id, username, firstname FROM users WHERE user_id = $1", id)
	if err := row.Scan(&user.ID, &user.Username, &user.FirstName); err != nil {
		return user, fmt.Errorf("can;t scan user: %s", err.Error())
	}
	return user, nil
}

func (u *UserRepo) GetAllUsers() ([]models.User, error) {
	u.Mutex.Lock()
	defer u.Mutex.Unlock()
	rows, err := u.db.Query(context.Background(),
		"SELECT user_id, username, firstname FROM users")

	if err != nil {
		return nil, fmt.Errorf("can't select all users: %s", err.Error())
	}

	defer rows.Close()

	var result []models.User

	for rows.Next() {
		user := models.User{}
		if err := rows.Scan(&user.ID, &user.Username, &user.FirstName); err != nil {
			return nil, fmt.Errorf("can't scan user: %s", err.Error())
		}
		result = append(result, user)
	}

	return result, nil

}

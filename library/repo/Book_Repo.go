package repo

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"gitlab.com/kuzmem/go-library/library/models"
	"sync"
)

type BookRepo struct {
	db *pgx.Conn
	*sync.Mutex
}

type BookRepoer interface {
	CreateBook(title string, user_id, author_id int) error
	GetBookByID(id int) (models.Book, error)
	GetBookByTitle(title string) (models.Book, error)
	GetBooksByAuthorID(author_id int) ([]models.Book, error)
	GetBooksByUserID(user_id int) ([]models.Book, error)
	GetAllBooks() ([]models.Book, error)
	TakeBook(book_id, user_id int) error
	ReturnBook(book_id int) error
}

func NewBookRepo(db *pgx.Conn) *BookRepo {
	return &BookRepo{
		db:    db,
		Mutex: &sync.Mutex{},
	}
}

func (b *BookRepo) CreateBook(title string, user_id, author_id int) error {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	// проверка на наличие такого автора
	//_, err := b.db.Query(context.Background(),
	//	"SELECT * FROM authors WHERE author_id = $1 ", author_id)
	//if err != nil {
	//	return err
	//}
	_, err := b.db.Exec(context.Background(),
		"INSERT INTO books (title, user_id, author_id) VALUES ($1, $2, $3)",
		title, user_id, author_id)
	if err != nil {
		return err
	}
	return nil
}

func (b *BookRepo) GetBookByID(id int) (models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	row := b.db.QueryRow(context.Background(),
		"SELECT books_id, title, user_id, author_id FROM books WHERE books_id = $1", id)
	var book models.Book
	if err := row.Scan(&book.ID, &book.Title, &book.User_ID, &book.Author_ID); err != nil {
		return book, fmt.Errorf("can't scan book: %s", err.Error())
	}
	return book, nil
}

func (b *BookRepo) GetBooksByAuthorID(author_id int) ([]models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	rows, err := b.db.Query(context.Background(),
		"SELECT books_id, title, user_id, author_id FROM books WHERE author_id = $1", author_id)
	if err != nil {
		return nil, fmt.Errorf("can't select books by author_id: %s", err.Error())
	}
	defer rows.Close()
	var result []models.Book
	for rows.Next() {
		book := models.Book{}
		if err := rows.Scan(&book.ID, &book.Title, &book.User_ID, &book.Author_ID); err != nil {
			return nil, fmt.Errorf("can't scan books by author_id: %s", err.Error())
		}
		result = append(result, book)
	}
	return result, nil
}

func (b *BookRepo) GetBooksByUserID(user_id int) ([]models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	rows, err := b.db.Query(context.Background(),
		"SELECT books_id, title, user_id, author_id FROM books WHERE user_id = $1", user_id)
	if err != nil {
		return nil, fmt.Errorf("can't select books by author_id: %s", err.Error())
	}
	defer rows.Close()
	var result []models.Book
	for rows.Next() {
		book := models.Book{}
		if err := rows.Scan(&book.ID, &book.Title, &book.User_ID, &book.Author_ID); err != nil {
			return nil, fmt.Errorf("can't scan books by user_id: %s", err.Error())
		}
		result = append(result, book)
	}
	return result, nil
}

func (b *BookRepo) GetBookByTitle(title string) (models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	row := b.db.QueryRow(context.Background(),
		"SELECT books_id, title, user_id, author_id FROM books WHERE title = $1", title)
	var book models.Book
	if err := row.Scan(&book.ID, &book.Title, &book.User_ID, &book.Author_ID); err != nil {
		return book, fmt.Errorf("can't scan book: %s", err.Error())
	}
	return book, nil
}

func (b *BookRepo) GetAllBooks() ([]models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	rows, err := b.db.Query(context.Background(),
		"SELECT books_id, title, user_id, author_id FROM books")
	if err != nil {
		return nil, fmt.Errorf("can't select all books: %s", err.Error())
	}
	defer rows.Close()
	var result []models.Book
	for rows.Next() {
		book := models.Book{}
		if err := rows.Scan(&book.ID, &book.Title, &book.User_ID, &book.Author_ID); err != nil {
			return nil, fmt.Errorf("can't scan all books: %s", err.Error())
		}
		result = append(result, book)
	}
	return result, nil
}

func (b *BookRepo) TakeBook(book_id, user_id int) error {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	_, err := b.db.Exec(context.Background(),
		"UPDATE books SET user_id = $1 WHERE books_id = $2",
		user_id, book_id)
	if err != nil {
		return fmt.Errorf("can't take the book: %s", err.Error())
	}
	return nil
}

func (b *BookRepo) ReturnBook(book_id int) error {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	//_, err := b.db.Query(context.Background(),
	//	"SELECT * FROM books WHERE books_id = $1",
	//	book_id)
	//if err != nil {
	//	return fmt.Errorf("can't find book: %s", err.Error())
	//}
	_, err := b.db.Exec(context.Background(),
		"UPDATE books SET user_id = 0 WHERE books_id = $1",
		book_id)
	if err != nil {
		return fmt.Errorf("can't return book: %s", err.Error())
	}
	return nil
}
